v1.5.7
- add launcher icon

v1.5.6
- remove possibility to open links in incognito tab for Icecat and Fennec for versions based on Firefox 79 and above
- minor improvements

v1.5.5
- remove possibility to open links in incognito tab for Firefox 79 and above
- improve icons
- improve RTL text support
- improve French translation (Thanks to xin!)
- minor improvements

v1.5.4
- fix export of signed APK ("Could not find com.android.tools.lint:lint-gradle:26.1.2.")

v1.5.3
- remove flicker in info screen for old Android versions (Gingerbread, ...)
- minor code improvements

v1.5.2
- add Google repo for Gradle plugin in order for F-Droid to be able to build the project

v1.5.1
- try to fix mysterious runtime exception

v1.5.0
- add possibility to open links in incognito tab if supported browser is installed (Thanks to Ildar Mulyukov for the idea.)
- Wikipedia links which end with ")" are opened correctly now
- improve Russian translation (Thanks to Ildar Mulyukov for the PR.)
- set max. API level to 27

v1.4.6
- improve typography (Thanks to Nave for the PR.)

v1.4.5
- add "<" and ">" to the characters to be ignored (Thanks to SN for the bug report.)

v1.4.4.x
- downgrade gradle to last stable version
- try to persuade F-Droid to build new release

v1.4.4
- add checks to prevent crashes if device can't open links to video or source code
- update gradle to 3.0.0-alpha3

v1.4.3
- add Hindi translation
- set max. API level to 25 (Android 7.1)
- update gradle to 2.2.3

v1.4.2
- set max. API level to 24 (Android 7)
- minor code changes due to new max API level
- update gradle to 2.1.3

v1.4.1
- set minimum API level to 3 (Android 1.5)
- minor code improvements
- try to improve Chinese translations (add traditional and simplified) in addition to regular zn-locale

v1.4.0
- improve icons
- remove entry from launcher, Activity can only be accessed via "open"-button in "Play Store"- oder "F-Droid"-app
- change max. API level to 23 (Android 6)

v1.3.1
- update source code repository URL (moved from Gitorious to GitLab)
- improve layout
- changed max. API level to 22 (Android 5.1)

v1.3
- support for Material design (Android 5.0 and above)
- improve URL parsing (thanks to R. Wong)

v1.2.1
- remove flicker when links are opened

v1.2
- add translations for the following languages: Arabic, Bulgarian, Catalan, Czech, Danish, Spanish, Finnish, French, Italian, Japanese, Dutch, Norwegian, Polish, Portuguese, Russian, Swedish, Thai, Chinese

v1.1
- brackets and punctuation characters enclosing URLs will be ignored (Thanks to DB for the idea.)

v1.0.2
- more UI improvements, friendlier info screen

v1.0.1
- minor UI improvements in info screen

v1.0
- initial release


