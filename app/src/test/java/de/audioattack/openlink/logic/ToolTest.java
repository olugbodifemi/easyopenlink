package de.audioattack.openlink.logic;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ToolTest {

    @Test
    void testEmptyInput() {

        final String input = "";

        assertEquals(Collections.emptySet(), Tool.getUris(input));
    }

    @Test
    void testNullInput() {

        assertEquals(Collections.emptySet(), Tool.getUris(null));
    }

    @Test
    void testSimpleUrlInText() {

        final String input = "blah blah http://example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testSimpleUrlWithTrailingSlashInText() {

        final String input = "blah blah http://example.org/ blah blah";
        final Set<String> output = toSet("http://example.org/");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testSimpleUrlsInText() {

        final String input = "blah blah http://example.org\n\nhttp://www.example.net blah blah";
        final Set<String> output = toSet("http://example.org", "http://www.example.net");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testUrlWithPathInText() {

        final String input = "blah blah http://example.org/test/test.html blah blah";
        final Set<String> output = toSet("http://example.org/test/test.html");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testUrlsWithPathInText() {

        final String input = "blah blah http://example.org/test/test.html blah http://example.net/html/index.pl blah";
        final Set<String> output = toSet("http://example.org/test/test.html", "http://example.net/html/index.pl");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testIncompleteSimpleUrlInText() {

        final String input = "blah blah example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testIncompleteSimpleUrlsInText() {

        final String input = "blah blah example.org blah example.net blah";
        final Set<String> output = toSet("http://example.org", "http://example.net");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testIncompleteUrlsInText() {

        final String input = "blah blah example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testWikipediaUrlInText() {

        final String input = "blah blah https://de.wikipedia.org/wiki/Walter_Bode_(Politiker) blah blah";
        final Set<String> output = toSet("https://de.wikipedia.org/wiki/Walter_Bode_(Politiker)");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testNotWikipediaUrlInText() {

        final String input = "blah blah (home: https://example.org/) blah blah";
        final Set<String> output = toSet("https://example.org/");

        assertEquals(output, Tool.getUris(input));
    }

    // see https://codeberg.org/marc.nause/easyopenlink/issues/6
    @Test
    void testLinkFollowedByClosingBracketAndNoSpace() {

        final String input = "Lob vom netzpolitisch engagierten Verein „Digitalcourage“ (https://digitalcourage.de/digitale-selbstverteidigung/es-geht-auch-ohne-google-alternative-suchmaschinen). Nachdem sonst immer";
        final Set<String> output = toSet("https://digitalcourage.de/digitale-selbstverteidigung/es-geht-auch-ohne-google-alternative-suchmaschinen");

        assertEquals(output, Tool.getUris(input));
    }

    // see https://codeberg.org/marc.nause/easyopenlink/issues/6
    @Test
    void testLinkWitrhTrailingSlashFollowedByClosingBracketAndNoSpace() {

        final String input = "bekanntlich CO2-neutral (https://www.hetzner.de/unternehmen/umweltschutz/), dennoch wollen wir";
        final Set<String> output = toSet("https://www.hetzner.de/unternehmen/umweltschutz/");

        assertEquals(output, Tool.getUris(input));
    }

    @Test
    void testWikipediaLinkWitrhTrailingSlashFollowedByClosingBracketAndNoSpace() {

        final String input = "bekanntlich CO2-neutral (https://de.wikipedia.org/wiki/Walter_Bode_(Politiker)), dennoch wollen wir";
        final Set<String> output = toSet("https://de.wikipedia.org/wiki/Walter_Bode_(Politiker)");

        assertEquals(output, Tool.getUris(input));
    }

    private static Set<String> toSet(final String... strings) {
        return new HashSet<>(Arrays.asList(strings));
    }
}
