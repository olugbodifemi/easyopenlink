package de.audioattack.openlink.conf;

import android.content.pm.PackageInfo;

import org.junit.jupiter.api.Test;

import de.audioattack.openlink.IncognitoBrowsers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class RequirementsCheckerTest {

    @Test
    void getIncognitoBrowserTestFirefoxTooOld() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.mozilla.firefox";
        packageInfo.versionCode = 123;
        packageInfo.setLongVersionCode(123);

        assertNull(RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

    @Test
    void getIncognitoBrowserTestFirefoxTooNew() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.mozilla.firefox";
        packageInfo.versionCode = Integer.MAX_VALUE;
        packageInfo.setLongVersionCode(Integer.MAX_VALUE);

        assertNull(RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

    @Test
    void getIncognitoBrowserTestFirefox() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.mozilla.firefox";
        packageInfo.versionCode = 2015503969;
        packageInfo.setLongVersionCode(2015503969);

        assertEquals(IncognitoBrowsers.FIREFOX, RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

    @Test
    void getIncognitoBrowserTestFennecFdroid() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.mozilla.fennec_fdroid";
        packageInfo.versionCode = 550000;
        packageInfo.setLongVersionCode(550000);

        assertEquals(IncognitoBrowsers.FENNEC_FDROID, RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

    @Test
    void getIncognitoBrowserTestIcecat() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.gnu.icecat";
        packageInfo.versionCode = 550000;
        packageInfo.setLongVersionCode(550000);

        assertEquals(IncognitoBrowsers.ICECAT, RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

    @Test
    void getIncognitoBrowserTestJelly() {

        final PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = "org.lineageos.jelly";
        packageInfo.versionCode = 2;
        packageInfo.setLongVersionCode(2);

        assertEquals(IncognitoBrowsers.JELLY, RequirementsChecker.getIncognitoBrowser(packageInfo));
    }

}
