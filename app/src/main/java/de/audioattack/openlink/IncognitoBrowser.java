/*
 * Copyright 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink;

public class IncognitoBrowser {

    public final String packageName;
    public final String browserActivityName;
    public final String incognitoExtra;
    public final long minVersionCode;
    public final long maxVersionCode;

    public IncognitoBrowser(final String packageName,
                            final String browserActivityName,
                            final int minVersionCode,
                            final int maxVersionCode,
                            final String incognitoExtra) {
        this.packageName = packageName;
        this.browserActivityName = browserActivityName;
        this.minVersionCode = minVersionCode;
        this.maxVersionCode = maxVersionCode;
        this.incognitoExtra = incognitoExtra;
    }

    public IncognitoBrowser(final String packageName,
                            final String browserActivityName,
                            final int minVersionCode,
                            final String incognitoExtra) {
        this.packageName = packageName;
        this.browserActivityName = browserActivityName;
        this.minVersionCode = minVersionCode;
        this.maxVersionCode = Integer.MAX_VALUE;
        this.incognitoExtra = incognitoExtra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncognitoBrowser browser = (IncognitoBrowser) o;

        if (minVersionCode != browser.minVersionCode) return false;
        if (maxVersionCode != browser.maxVersionCode) return false;
        if (packageName != null ? !packageName.equals(browser.packageName) : browser.packageName != null)
            return false;
        if (browserActivityName != null ? !browserActivityName.equals(browser.browserActivityName) : browser.browserActivityName != null)
            return false;
        return incognitoExtra != null ? incognitoExtra.equals(browser.incognitoExtra) : browser.incognitoExtra == null;
    }

    @Override
    public int hashCode() {
        int result = packageName != null ? packageName.hashCode() : 0;
        result = 31 * result + (browserActivityName != null ? browserActivityName.hashCode() : 0);
        result = 31 * result + (incognitoExtra != null ? incognitoExtra.hashCode() : 0);
        result = 31 * result + (int) (minVersionCode ^ (minVersionCode >>> 32));
        result = 31 * result + (int) (maxVersionCode ^ (maxVersionCode >>> 32));
        return result;
    }
}
