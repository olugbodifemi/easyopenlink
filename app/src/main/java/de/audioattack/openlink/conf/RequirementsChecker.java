/*
 * Copyright 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.conf;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.pm.PackageInfoCompat;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import de.audioattack.openlink.IncognitoBrowser;
import de.audioattack.openlink.IncognitoBrowsers;

public class RequirementsChecker {

    private static final Collection<IncognitoBrowser> PACKAGES = Arrays.asList(
            IncognitoBrowsers.FIREFOX,
            IncognitoBrowsers.FIREFOX_LITE,
            IncognitoBrowsers.FENNEC_FDROID,
            IncognitoBrowsers.ICECAT,
            IncognitoBrowsers.JELLY,
            IncognitoBrowsers.JELLY_PLAYSTORE,
            IncognitoBrowsers.JQUARKS,
            IncognitoBrowsers.LIGHTNING,
            IncognitoBrowsers.MIDORI
    );

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private RequirementsChecker() {
    }

    public static boolean isIncognitoBrowserInstalled(@NonNull final Context context) {

        return getIncognitoBrowsers(context).size() > 0;
    }

    public static Collection<IncognitoBrowser> getIncognitoBrowsers(@NonNull final Context context) {
        final PackageManager pm = context.getApplicationContext().getPackageManager();

        final Collection<PackageInfo> applications = pm.getInstalledPackages(PackageManager.GET_META_DATA);
        final Collection<IncognitoBrowser> incognitoBrowsers = new HashSet<>();

        for (final PackageInfo applicationInfo : applications) {
            final IncognitoBrowser incognitoBrowser = getIncognitoBrowser(applicationInfo);
            if (incognitoBrowser != null) {
                incognitoBrowsers.add(incognitoBrowser);
            }
        }

        return incognitoBrowsers;
    }

    public static IncognitoBrowser getIncognitoBrowser(@Nullable final PackageInfo packageInfo) {

        return packageInfo == null ? null : getIncognitoBrowser(packageInfo.packageName, PackageInfoCompat.getLongVersionCode(packageInfo));
    }

    private static IncognitoBrowser getIncognitoBrowser(@NonNull final String packageName, final long versionCode) {

        for (final IncognitoBrowser browser : PACKAGES) {
            if (browser.packageName.equals(packageName) && versionCode >= browser.minVersionCode && versionCode <= browser.maxVersionCode) {
                return browser;
            }
        }

        return null;
    }

}



