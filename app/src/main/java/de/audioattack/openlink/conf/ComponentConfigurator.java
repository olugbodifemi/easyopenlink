/*
 * Copyright 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.conf;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;

public class ComponentConfigurator {

    private static final String TAG = ComponentConfigurator.class.getSimpleName();

    private final Context myContext;
    private final PackageManager myPackageManager;

    public ComponentConfigurator(@NonNull final Context context) {
        myContext = context.getApplicationContext();
        myPackageManager = myContext.getPackageManager();
    }


    public void setEnabled(final Class<? extends Activity> activityClass, final boolean enabled) {

        setEnabled(activityClass, getState(enabled));
    }

    private void setEnabled(@NonNull final Class<? extends Activity> clazz, final int state) {

        Log.d(TAG, "Setting " + clazz + " to " + (state == PackageManager.COMPONENT_ENABLED_STATE_ENABLED ? "enabled" : "disabled"));

        myPackageManager.setComponentEnabledSetting(
                new ComponentName(myContext, clazz),
                state,
                PackageManager.DONT_KILL_APP);
    }

    private static int getState(final boolean enabled) {

        return enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
    }

}
